const btns = document.querySelectorAll(".btn")

const btnArrayFull =  Array.from(btns);

function btnReset() {
    btnArrayFull.forEach((btn) => btn.style.backgroundColor = "black");
}

function btnPress(e) {
    const found = btnArrayFull.find((l) => l.textContent.toLowerCase() === e.key.toLowerCase());
    found.style.backgroundColor = "blue";
}

document.addEventListener("keypress", btnReset);
document.addEventListener("keyup", btnPress);